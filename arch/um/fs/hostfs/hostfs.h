/* 
 * Copyright (C) 2004 Jeff Dike (jdike@addtoit.com)
 * Licensed under the GPL
 */

#ifndef __UM_FS_HOSTFS
#define __UM_FS_HOSTFS

#include "os.h"

/* These are exactly the same definitions as in fs.h, but the names are 
 * changed so that this file can be included in both kernel and user files.
 */

#define HOSTFS_ATTR_MODE	1
#define HOSTFS_ATTR_UID 	2
#define HOSTFS_ATTR_GID 	4
#define HOSTFS_ATTR_SIZE	8
#define HOSTFS_ATTR_ATIME	16
#define HOSTFS_ATTR_MTIME	32
#define HOSTFS_ATTR_CTIME	64
#define HOSTFS_ATTR_ATIME_SET	128
#define HOSTFS_ATTR_MTIME_SET	256
#define HOSTFS_ATTR_FORCE	512	/* Not a change, but a change it */
#define HOSTFS_ATTR_ATTR_FLAG	1024

struct hostfs_iattr {
	unsigned int	ia_valid;
	mode_t		ia_mode;
	uid_t		ia_uid;
	gid_t		ia_gid;
	loff_t		ia_size;
	time_t		ia_atime;
	time_t		ia_mtime;
	time_t		ia_ctime;
	unsigned int	ia_attr_flags;
};

struct externfs_file_ops {
	int (*stat_file)(const char *path, void *mount, int *dev_out, 
			 unsigned long long *inode_out, int *mode_out, 
			 int *nlink_out, int *uid_out, int *gid_out, 
			 unsigned long long *size_out, 
			 unsigned long *atime_out, unsigned long *mtime_out, 
			 unsigned long *ctime_out, int *blksize_out, 
			 unsigned long long *blocks_out);
	int (*file_type)(const char *path, int *rdev, void *mount);
	int (*access_file)(char *path, int uid, int gid, int r, int w, int x, 
			   void *mount);
	int (*open_file)(char *path, int uid, int gid, int r, int w, 
			 void *mount);
	void *(*open_dir)(char *path, int uid, int gid, int *err_out, 
			  void *mount);
	char *(*read_dir)(void *stream, unsigned long long *pos, 
			  unsigned long long *ino_out, int *len_out, 
			  void *mount);
	int (*read_file)(int fd, unsigned long long offset, char *buf,
			 int len, void (*completion)(char *, int, void *), 
			 void *arg, void *mount);
	int (*write_file)(int fd, unsigned long long offset, const char *buf, 
			  int len, void *mount);
	int (*map_file_page)(int fd, unsigned long long offset, char *buf, 
			     int w, void *mount);
	void (*close_file)(void *stream, unsigned long long size, void *mount);
	void (*close_dir)(void *stream, void *mount);
	int (*file_create)(char *path, int uid, int gid, int ur, int uw, int ux,
			   int gr, int gw, int gx, int or, int ow, int ox, 
			   void *mount);
	int (*set_attr)(const char *path, struct hostfs_iattr *attrs, 
			void *mount);
	int (*make_symlink)(const char *from, const char *to, int uid, int gid,
			    void *mount);
	int (*unlink_file)(const char *path, void *mount);
	int (*mkdir)(const char *path, int mode, int uid, int gid, void *mount);
	int (*rmdir)(const char *path, int uid, int gid, void *mount);
	int (*mknod)(const char *path, int uid, int gid, int ur, int uw, int ux,
		     int gr, int gw, int gx, int or, int ow, int ox, int type,
		     int dev, void *mount);
	int (*link_file)(const char *to, const char *from, int uid, int gid, 
			 void *mount);
	int (*readlink)(char *path, int uid, int gid, char *buf, int size, 
			void *mount);
	int (*rename_file)(char *from, char *to, void *mount);
	int (*statfs)(long *bsize_out, long long *blocks_out, 
		      long long *bfree_out, long long *bavail_out, 
		      long long *files_out, long long *ffree_out,
		      void *fsid_out, int fsid_size, long *namelen_out, 
		      long *spare_out, void *mount);
	int (*truncate_file)(int fd, __u64 size, void *m);
};

struct externfs_mount_ops {
	struct externfs_file_ops *(*mount)(char *mount_arg, 
					   void **mount_data_out);
};

#define HOSTFS_BUFSIZE 64

extern int register_externfs(char *name, struct externfs_mount_ops *ops);
extern void unregister_externfs(char *name);

extern char *generic_root_filename(char *mount_arg);
extern void host_close_file(void *stream);
extern int host_read_file(int fd, unsigned long long offset, char *buf, 
			  int len);
extern int host_write_file(int fd, unsigned long long offset, const char *buf, 
			   int len);
extern int host_open_file(const char *path[], int r, int w);
extern void *host_open_dir(const char *path[], int *err_out);
extern char *host_read_dir(void *stream, unsigned long long *pos, 
			   unsigned long long *ino_out, int *len_out);
extern void host_close_dir(void *stream);
extern int host_file_type(const char *path[], int *rdev);
extern int host_root_filename(char *mount_arg, void **mount_data_out);
extern char *get_path(const char *path[], char *buf, int size);
extern void free_path(const char *buf, char *tmp);
extern int host_file_create(const char *path[], int ur, int uw, int ux, 
			    int gr, int gw, int gx, int or, int ow, int ox);
extern int host_set_attr(const char *path[], struct hostfs_iattr *attrs);
extern int host_make_symlink(const char *from[], const char *to);
extern int host_unlink_file(const char *path[]);
extern int host_mkdir(const char *path[], int mode);
extern int host_rmdir(const char *path[]);
extern int host_link_file(const char *to[], const char *from[]);
extern int host_readlink(const char *path[], char *buf, int size);
extern int host_rename_file(const char *from[], const char *to[]);
extern int host_statfs(const char *path[], long *bsize_out, 
		       long long *blocks_out, long long *bfree_out, 
		       long long *bavail_out, long long *files_out, 
		       long long *ffree_out, void *fsid_out, int fsid_size, 
		       long *namelen_out, long *spare_out);
extern int host_stat_file(const char *path[], int *dev_out, 
			  unsigned long long *inode_out, int *mode_out, 
			  int *nlink_out, int *uid_out, int *gid_out, 
			  unsigned long long *size_out, 
			  unsigned long *atime_out, unsigned long *mtime_out,
			  unsigned long *ctime_out, int *blksize_out,
			  unsigned long long *blocks_out);

extern char *generic_host_read_dir(void *stream, unsigned long long *pos, 
			      unsigned long long *ino_out, int *len_out, 
			      void *mount);
extern int generic_host_read_file(int fd, unsigned long long offset, char *buf,
			     int len, void *mount);
extern int generic_host_write_file(int fd, unsigned long long offset, 
			      const char *buf, int len, void *mount);
extern void generic_host_close_file(void *stream, unsigned long long size,
				    void *mount);
extern void generic_host_close_dir(void *stream, void *mount);
extern int generic_host_truncate_file(int fd, __u64 size, void *m);

#endif

/*
 * Overrides for Emacs so that we follow Linus's tabbing style.
 * Emacs will notice this stuff at the end of the file and automatically
 * adjust the settings for this buffer only.  This must remain at the end
 * of the file.
 * ---------------------------------------------------------------------------
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
