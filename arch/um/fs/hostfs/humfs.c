/* 
 * Copyright (C) 2004 Jeff Dike (jdike@addtoit.com)
 * Licensed under the GPL
 */

#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/sched.h>
#include <linux/stat.h>
#include <linux/tqueue.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/kdev_t.h>
#include <asm/irq.h>
#include "hostfs.h"
#include "user.h"
#include "mem.h"
#include "os.h"
#include "mode.h"
#include "aio.h"
#include "irq_user.h"
#include "irq_kern.h"

#define HUMFS_VERSION 1

struct humfs {
	__u64 used;
	__u64 total;
	char *data;
	char *metadata;
	int mmap;
};

static int open_meta_file(const char *path[])
{
	char tmp[HOSTFS_BUFSIZE];
	char *file = get_path(path, tmp, sizeof(tmp));
	int fd = -ENOMEM;

	if(file == NULL)
		goto out;
	
	/* of_rdwr makes the open fail if it's a directory, otherwise it
	 * would fail at the first read, which is less convenient.
	 */
	fd = os_open_file(file, of_rdwr(OPENFLAGS()), 0);
	if(fd == -EISDIR){
		const char *dir_path[] = { file, "metadata", NULL };
		char dir_tmp[HOSTFS_BUFSIZE], *dir_file;

		dir_file = get_path(dir_path, dir_tmp, sizeof(dir_tmp));
		fd = os_open_file(dir_file, of_rdwr(OPENFLAGS()), 0);
		free_path(dir_file, dir_tmp);
	}
	else if(fd < 0)
		goto out;

	free_path(file, tmp);
 out:
	return(fd);
}

static int ownerships(const char *path[], int *uid_out, int *gid_out, 
		      char *type_out, int *maj_out, int *min_out)
{
	char buf[sizeof("nnnnnnnnnn mmmmmmmmmm x nnn mmm\n")];
	int err = -ENOMEM, fd, n, maj, min;
	char type;

	fd = open_meta_file(path);
	if(fd < 0){
		err = fd;
		goto out;
	}

	err = os_read_file(fd, buf, sizeof(buf) - 1);
	if(err < 0)
		goto out_close;

	buf[err] = '\0';
	err = 0;

	n = sscanf(buf, "%d %d %c %d %d", uid_out, gid_out, &type, &maj, &min);
	if(n == 2){
		maj = -1;
		min = -1;
		type = 0;
		err = 0;
	}
	else if(n != 5)
		err = -EINVAL;

	if(type_out != NULL)
		*type_out = type;
	if(maj_out != NULL)
		*maj_out = maj;
	if(min_out != NULL)
		*min_out = min;

 out_close:
	os_close_file(fd);
 out:
	return(err);
}

static int stat_file(const char *path, void *m, int *dev_out, 
		     unsigned long long *inode_out, int *mode_out, 
		     int *nlink_out, int *uid_out, int *gid_out, 
		     unsigned long long *size_out, unsigned long *atime_out,
		     unsigned long *mtime_out, unsigned long *ctime_out,
		     int *blksize_out, unsigned long long *blocks_out)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, path, NULL };
	const char *metadata_path[3] = { mount->metadata, path, NULL };
	int err, mode;
	char type;

	err = host_stat_file(data_path, dev_out, inode_out, mode_out, 
			     nlink_out, uid_out, gid_out, size_out, 
			     atime_out, mtime_out, ctime_out, blksize_out, 
			     blocks_out);
	if(err)
		return(err);

	err = ownerships(metadata_path, uid_out, gid_out, &type, NULL, NULL);
	if(err)
		return(err);

	mode = 0;
	switch(type){
	case 'c':
		*mode_out = S_IFCHR;
		break;
	case 'b':
		mode = S_IFBLK;
		break;
	case 's':
		mode = S_IFSOCK;
		break;
	default:
		break;
	}

	if(mode != 0)
		*mode_out = (*mode_out & ~S_IFMT) | mode;

	return(0);
}

static int meta_type(const char *path[], int *dev_out)
{
	int err, uid, gid, type, maj, min;
	char c;

	err = ownerships(path, &uid, &gid, &c, &maj, &min);
	if(err)
		return(err);

	if(c == 0)
		return(0);

	*dev_out = MKDEV(maj, min);
	switch(c){
	case 'c':
		type = OS_TYPE_CHARDEV;
		break;
	case 'b':
		type = OS_TYPE_BLOCKDEV;
		break;
	case 'p':
		type = OS_TYPE_FIFO;
		break;
	case 's':
		type = OS_TYPE_SOCK;
		break;
	default:
		type = -EINVAL;
		break;
	}

	return(type);
}

static int file_type(const char *path, int *dev_out, void *m)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, path, NULL };
	const char *metadata_path[3] = { mount->metadata, path, NULL };
	int type;

	type = meta_type(metadata_path, dev_out);
	if(type != 0)
		return(type);

	return(host_file_type(data_path, dev_out));
}

static int open_file(char *path, int uid, int gid, int r, int w, void *m)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, path, NULL };

	return(host_open_file(data_path, r, 1));
}

static void *open_dir(char *path, int uid, int gid, int *err_out, void *m)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, path, NULL };

	return(host_open_dir(data_path, err_out));
}

LIST_HEAD(humfs_replies);

struct humfs_aio {
	struct aio_context aio;
	struct list_head list;
	void (*completion)(char *, int, void *);
	char *buf;
	int err;
	void *data;
};

static int humfs_reply_fd = -1;

void humfs_task_proc(void *unused)
{
	struct humfs_aio *aio;
	unsigned long flags;
	int done;

	do {
		save_flags(flags);
		aio = list_entry(humfs_replies.next, struct humfs_aio, list);
		list_del(&aio->list);
		done = list_empty(&humfs_replies);
		restore_flags(flags);

		(*aio->completion)(aio->buf, aio->err, aio->data);
		kfree(aio);
	} while(!done);
}

struct tq_struct humfs_task = {
	.routine	= humfs_task_proc,
	.data 		= NULL
};

static void humfs_interrupt(int irq, void *dev_id, struct pt_regs *unused)
{
	struct aio_thread_reply reply;
	struct humfs_aio *aio;
	int err, fd = (int) dev_id;

	while(1){
		err = os_read_file(fd, &reply, sizeof(reply));
		if(err < 0){
			if(err == -EAGAIN)
				break;
			printk("humfs_interrupt - read returned err %d\n", 
			       -err);
			return;
		}
		aio = reply.data;
		aio->err = reply.err;
		list_add(&aio->list, &humfs_replies);
	}

	if(!list_empty(&humfs_replies))
		schedule_task(&humfs_task);
	reactivate_fd(fd, HUMFS_IRQ);
}

static int init_humfs_aio(void)
{
	int fds[2], err;

	err = os_pipe(fds, 1, 1);
	if(err){
		printk("init_humfs_aio - pipe failed, err = %d\n", -err);
		goto out;
	}

	err = um_request_irq(HUMFS_IRQ, fds[0], IRQ_READ, humfs_interrupt,
			     SA_INTERRUPT | SA_SAMPLE_RANDOM, "humfs", 
			     (void *) fds[0]);
	if(err){
		printk("init_humfs_aio - : um_request_irq failed, err = %d\n",
		       err);
		goto out_close;
	}

	humfs_reply_fd = fds[1];
	goto out;
	
 out_close:
	os_close_file(fds[0]);
	os_close_file(fds[1]);
 out:
	return(0);
}

__initcall(init_humfs_aio);

static int read_file(int fd, unsigned long long offset, char *buf, int len,
		     void (*completion)(char *, int, void *), void *arg, 
		     void *mount)
{
	struct humfs_aio *aio;
	int err = -ENOMEM;

	aio = um_kmalloc(sizeof(aio));
	if(aio == NULL)
		goto out;
	*aio = ((struct humfs_aio) { .completion	= completion,
				     .buf		= buf,
				     .data		= arg });

	err = submit_aio(AIO_READ, fd, buf, len, offset, humfs_reply_fd, aio);
	if(err)
		(*completion)(buf, err, arg);

 out:
	return(err);
}

static int map_file_page(int fd, unsigned long long offset, char *buf, int w,
			 void *m)
{
	unsigned long long size, need;
	int err;

	err = os_fd_size(fd, &size);
	if(err)
		return(err);

	need = offset + PAGE_SIZE;
	if(size < need){
		err = os_ftruncate(fd, need);
		if(err)
			return(err);
	}
	
	return(physmem_subst_mapping(buf, fd, offset, w));
}

static void close_file(void *stream, unsigned long long size, void *m)
{
	int fd = *((int *) stream);

	physmem_forget_descriptor(fd);
	os_ftruncate(fd, size);
	os_close_file(fd);
}

static int create_shadow_file(const char *path[], int uid, int gid)
{
	char tmp[HOSTFS_BUFSIZE];
	char *file = get_path(path, tmp, sizeof(tmp));
	char buf[sizeof("nnnnnnnnnn mmmmmmmmmm")];
	int fd, err = -ENOMEM;

	if(file == NULL)
		goto out;

	fd = os_open_file(file, of_write(of_create(OPENFLAGS())), 0644);
	if(fd < 0){
		err = fd;
		goto out;
	}

	sprintf(buf, "%d %d\n", uid, gid);
	err = os_write_file(fd, buf, strlen(buf));
	if(err > 0)
		err = 0;

	os_close_file(fd);
 out:
	free_path(file, tmp);
	return(err);
}

static int remove_shadow_file(const char *path[])
{
	char tmp[HOSTFS_BUFSIZE];
	char *file = get_path(path, tmp, sizeof(tmp));
	int err = -ENOMEM;

	if(file == NULL)
		goto out;

	err = os_remove_file(file);

 out:
	free_path(file, tmp);
	return(err);
}

static int create_shadow_directory(const char *path[], int uid, int gid)
{
	char dir_tmp[HOSTFS_BUFSIZE], file_tmp[HOSTFS_BUFSIZE];
	const char *metadata_path[] = { NULL, "metadata", NULL };
	char *file, dir_meta[sizeof("mmmmmmmmm nnnnnnnnnn\n")];
	int err, fd;

	err = host_mkdir(path, 0755);
	if(err)
		return(err);

	metadata_path[0] = get_path(path, dir_tmp, sizeof(dir_tmp));
	if(metadata_path[0] == NULL)
		return(-ENOMEM);

	err = -ENOMEM;
	file = get_path(metadata_path, file_tmp, sizeof(file_tmp));
	if(file == NULL)
		goto out;

	fd = os_open_file(file, of_create(of_rdwr(OPENFLAGS())), 0644);
	if(fd < 0){
		err = fd;
		goto out_free;
	}

	sprintf(dir_meta, "%d %d\n", uid, gid);
	err = os_write_file(fd, dir_meta, strlen(dir_meta));
	if(err > 0)
		err = 0;

	os_close_file(fd);

 out_free:
	free_path(file, file_tmp);
 out:
	free_path(metadata_path[0], dir_tmp);
	return(err);
}

static int remove_shadow_directory(const char *path[])
{
	char dir_tmp[HOSTFS_BUFSIZE], file_tmp[HOSTFS_BUFSIZE], *file;
	const char *metadata_path[] = { NULL, "metadata", NULL };
	int err;

	metadata_path[0] = get_path(path, dir_tmp, sizeof(dir_tmp));
	if(metadata_path[0] == NULL)
		return(-ENOMEM);

	err = -ENOMEM;
	file = get_path(metadata_path, file_tmp, sizeof(file_tmp));
	if(file == NULL)
		goto out;

	err = os_remove_file(file);
	if(err)
		goto out_free;

	err = os_remove_dir(metadata_path[0]);

 out_free:
	free_path(file, file_tmp);
 out:
	free_path(metadata_path[0], dir_tmp);
	return(err);
}

static int file_create(char *path, int uid, int gid, int ur, int uw, int ux, 
		       int gr, int gw, int gx, int or, int ow, int ox, void *m)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, path, NULL };
	const char *metadata_path[3] = { mount->metadata, path, NULL };
	int err, fd;

	err = create_shadow_file(metadata_path, uid, gid);
	if(err)
		return(err);

	fd = host_file_create(data_path, ur, uw, ux, gr, gw, gx, or, ow, ox);
	if(fd < 0)
		remove_shadow_file(metadata_path);

	return(fd);
}

static int change_shadow_ownerships(const char *path[], int uid, int gid)
{
	char type;
	char buf[sizeof("nnnnnnnnnn mmmmmmmmmm x nnn mmm\n")];
	int err = -ENOMEM, old_uid, old_gid, fd, n, maj, min;

	fd = open_meta_file(path);
	if(fd < 0){
		err = fd;
		goto out;
	}

	err = os_read_file(fd, buf, sizeof(buf) - 1);
	if(err < 0)
		goto out_close;

	buf[err] = '\0';

	n = sscanf(buf, "%d %d %c %d %d\n", &old_uid, &old_gid, &type, 
		   &maj, &min);
	if((n != 2) && (n != 5)){
		err = -EINVAL;
		goto out_close;
	}

	if(uid == -1)
		uid = old_uid;
	if(gid == -1)
		gid = old_gid;

	if(n == 2)
		sprintf(buf, "%d %d\n", uid, gid);
	else
		sprintf(buf, "%d %d %c %d %d\n", uid, gid, type, maj, min);

	err = os_seek_file(fd, 0);
	if(err < 0)
		goto out_close;

	err = os_write_file(fd, buf, strlen(buf));
	if(err > 0)
		err = 0;

	err = os_truncate_fd(fd, strlen(buf));

 out_close:
	os_close_file(fd);
 out:
	return(err);
}

static int set_attr(const char *path, struct hostfs_iattr *attrs, void *m)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, path, NULL };
	const char *metadata_path[3] = { mount->metadata, path, NULL };
	int err;

	if(attrs->ia_valid & HOSTFS_ATTR_UID){
		err = change_shadow_ownerships(metadata_path, 
					       attrs->ia_uid, -1);
		if(err)
			return(err);
	}
	if(attrs->ia_valid & HOSTFS_ATTR_GID){
		err = change_shadow_ownerships(metadata_path, 
					       -1, attrs->ia_gid);
		if(err)
			return(err);
	}

	attrs->ia_valid &= ~(HOSTFS_ATTR_UID | HOSTFS_ATTR_GID);

	return(host_set_attr(data_path, attrs));
}

static int make_symlink(const char *from, const char *to, int uid, int gid,
			void *m)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, from, NULL };
	const char *metadata_path[3] = { mount->metadata, from, NULL };
	int err;

	err = create_shadow_file(metadata_path, uid, gid);
	if(err)
		return(err);

	err = host_make_symlink(data_path, to);
	if(err)
		remove_shadow_file(metadata_path);

	return(err);
}

static int unlink_file(const char *path, void *m)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, path, NULL };
	const char *metadata_path[3] = { mount->metadata, path, NULL };

	remove_shadow_file(metadata_path);

	return(host_unlink_file(data_path));
}

static int make_dir(const char *path, int mode, int uid, int gid, void *m)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, path, NULL };
	const char *metadata_path[3] = { mount->metadata, path, NULL };
	int err;

	err = create_shadow_directory(metadata_path, uid, gid);
	if(err)
		return(err);
	
	err = host_mkdir(data_path, mode);
	if(err)
		remove_shadow_directory(metadata_path);

	return(err);
}

static int rm_dir(const char *path, int uid, int gid, void *m)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, path, NULL };
	const char *metadata_path[3] = { mount->metadata, path, NULL };

	remove_shadow_directory(metadata_path);

	return(host_rmdir(data_path));
}

static int mk_nod(const char *path, int uid, int gid, int ur, int uw, int ux,
		  int gr, int gw, int gx, int or, int ow, int ox, int type,
		  int dev, void *m)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, path, NULL };
	const char *metadata_path[3] = { mount->metadata, path, NULL };
	int err, fd, maj = 0, min = 0;
	char t, buf[sizeof("nnnnnnnnnn mmmmmmmmmm x nnn mmm\n")], *file;
	char tmp[HOSTFS_BUFSIZE];

	err = host_file_create(data_path, ur, uw, ux, gr, gw, gx, or, ow, ox);
	if(err)
		goto out;

	switch(type){
	case S_IFCHR:
		t = 'c';
		maj = MAJOR(dev);
		min = MINOR(dev);
		break;
	case S_IFBLK:
		t = 'b';
		maj = MAJOR(dev);
		min = MINOR(dev);
		break;
	case S_IFIFO:
		t = 'p';
		break;
	case S_IFSOCK:
		t = 's';
		break;
	default:
		err = -EINVAL;
		printk("do_mknod - bad node type : %d\n", type);
		goto out_rm_host;
	}
		
	sprintf(buf, "%d %d %c %d %d\n", uid, gid, t, maj, min);

	err = -ENOMEM;
	file = get_path(metadata_path, tmp, sizeof(tmp));
	if(file == NULL)
		goto out_rm_host;

	fd = os_open_file(file, of_create(of_rdwr(OPENFLAGS())), 0644);
	if(fd < 0){
		err = fd;
		goto out_rm_host;
	}
	err = os_write_file(fd, buf, strlen(buf));
	if(err > 0)
		err = 0;

	os_close_file(fd);

	if(!err)
		return(0);

	remove_shadow_file(metadata_path);
	free_path(file, tmp);
 out_rm_host:
	host_unlink_file(data_path);
 out:
	return(err);
}

static int link_file(const char *to, const char *from, int uid, int gid, 
		     void *m)
{
	struct humfs *mount = m;
	const char *data_path_from[3] = { mount->data, from, NULL };
	const char *data_path_to[3] = { mount->data, to, NULL };
	const char *metadata_path_from[3] = { mount->metadata, from, NULL };
	int err;

	err = create_shadow_file(metadata_path_from, uid, gid);
	if(err)
		return(err);
	
	err = host_link_file(data_path_to, data_path_from);
	if(err)
		remove_shadow_file(metadata_path_from);

	return(err);
}

static int read_link(char *file, int uid, int gid, char *buf, int size, void *m)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, file, NULL };

	return(host_readlink(data_path, buf, size));
}

static int rename_file(char *from, char *to, void *m)
{
	struct humfs *mount = m;
	const char *data_path_from[3] = { mount->data, from, NULL };
	const char *data_path_to[3] = { mount->data, to, NULL };
	const char *metadata_path_from[3] = { mount->metadata, from, NULL };
	const char *metadata_path_to[3] = { mount->metadata, to, NULL };
	int err;

	err = host_rename_file(metadata_path_from, metadata_path_to);
	if(err)
		return(err);
	
	err = host_rename_file(data_path_from, data_path_to);
	if(err)
		host_rename_file(metadata_path_to, metadata_path_from);

	return(err);
}

static int stat_fs(long *bsize_out, long long *blocks_out, long long *bfree_out,
		   long long *bavail_out, long long *files_out, 
		   long long *ffree_out, void *fsid_out, int fsid_size, 
		   long *namelen_out, long *spare_out, void *m)
{
	struct humfs *mount = m;
	const char *data_path[3] = { mount->data, NULL };
	int err;

	/* XXX Needs to maintain this info as metadata */
	err = host_statfs(data_path, bsize_out, blocks_out, bfree_out, 
			  bavail_out, files_out, ffree_out, fsid_out, 
			  fsid_size, namelen_out, spare_out);
	if(err)
		return(err);

	*blocks_out = mount->total / *bsize_out;
	*bfree_out = (mount->total - mount->used) / *bsize_out;
	*bavail_out = (mount->total - mount->used) / *bsize_out;
	return(0);
}

static char *path(char *dir, char *file)
{
	int need_slash, len = strlen(dir) + strlen(file);
	char *new;

	need_slash = (dir[strlen(dir)] != '/');
	if(need_slash)
		len++;

	new = um_kmalloc(len + 1);
	if(new == NULL)
		return(NULL);

	strcpy(new, dir);
	if(need_slash)
		strcat(new, "/");
	strcat(new, file);

	return(new);
}

static int read_superblock(char *root, __u64 *used_out, __u64 *total_out)
{
	const char *path[] = { root, "superblock", NULL };
	char line[HOSTFS_BUFSIZE], *newline;
	int version, i, n, fd = host_open_file(path, 1, 0);

	if(fd < 0)
		return(fd);

	*used_out = 0;
	*total_out = 0;
	i = 0;
	while(1){
		n = os_read_file(fd, &line[i], sizeof(line) - i - 1);
		if((n == 0) && (i == 0))
			break;
		if(n < 0)
			return(n);

		if(n > 0)
			line[n + i] = '\0';

		newline = strchr(line, '\n');
		if(newline == NULL){
			printk("read_superblock - line too long : '%s'\n", 
			       line);
			return(-EINVAL);
		}
		newline++;

		if(sscanf(line, "version %d\n", &version) == 1){
			if(version != HUMFS_VERSION){
				printk("humfs version mismatch - want version "
				       "%d, got version %d.\n", HUMFS_VERSION,
				       version);
				return(-EINVAL);
			}
		}
		else if(sscanf(line, "used %Lu\n", used_out) == 1) ;
		else if(sscanf(line, "total %Lu\n", total_out) == 1) ;
		else {
			printk("read_superblock - bogus line : '%s'\n", line);
			return(-EINVAL);
		}

		i = newline - line;
		memmove(line, newline, sizeof(line) - i);
		i = strlen(line);
	}

	if(*used_out == 0){
		printk("read_superblock - used not specified or set to zero\n");
		return(-EINVAL);
	}
	if(*total_out == 0){
		printk("read_superblock - total not specified or set to "
		       "zero\n");
		return(-EINVAL);
	}
	if(*used_out > *total_out){
		printk("read_superblock - used is greater than total\n");
		return(-EINVAL);
	}

	return(0);
}

struct externfs_file_ops humfs_no_mmap_file_ops = {
	.stat_file		= stat_file,
	.file_type		= file_type,
	.access_file		= NULL,
	.open_file		= open_file,
	.open_dir		= open_dir,
	.read_dir		= generic_host_read_dir,
	.read_file		= read_file,
	.write_file		= generic_host_write_file,
	.map_file_page		= NULL,
	.close_file		= close_file,
	.close_dir		= generic_host_close_dir,
	.file_create		= file_create,
	.set_attr		= set_attr,
	.make_symlink		= make_symlink,
	.unlink_file		= unlink_file,
	.mkdir			= make_dir,
	.rmdir			= rm_dir,
	.mknod			= mk_nod,
	.link_file		= link_file,
	.readlink		= read_link,
	.rename_file		= rename_file,
	.statfs			= stat_fs,
	.truncate_file		= generic_host_truncate_file
};

struct externfs_file_ops humfs_mmap_file_ops = {
	.stat_file		= stat_file,
	.file_type		= file_type,
	.access_file		= NULL,
	.open_file		= open_file,
	.open_dir		= open_dir,
	.read_dir		= generic_host_read_dir,
	.read_file		= read_file,
	.write_file		= generic_host_write_file,
	.map_file_page		= map_file_page,
	.close_file		= close_file,
	.close_dir		= generic_host_close_dir,
	.file_create		= file_create,
	.set_attr		= set_attr,
	.make_symlink		= make_symlink,
	.unlink_file		= unlink_file,
	.mkdir			= make_dir,
	.rmdir			= rm_dir,
	.mknod			= mk_nod,
	.link_file		= link_file,
	.readlink		= read_link,
	.rename_file		= rename_file,
	.statfs			= stat_fs,
	.truncate_file		= generic_host_truncate_file
};

static struct externfs_file_ops *mount_fs(char *mount_arg, 
					  void **mount_data_out)
{
	char *root, *data, *metadata, *flags;
	struct humfs *mount;
	__u64 used, total;
	int err, do_mmap = 0;

	if(mount_arg == NULL){
		printk("humfs - no host directory specified\n");
		return(NULL);
	}

	mount = um_kmalloc(sizeof(mount));
	if(mount == NULL)
		goto err;

	flags = strchr((char *) mount_arg, ',');
	if(flags != NULL){
		do {
			*flags++ = '\0';

			if(!strcmp(flags, "mmap"))
				do_mmap = 1;

			flags = strchr(flags, ',');
		} while(flags != NULL);
	}

	err = host_root_filename(mount_arg, (void **) &root);
	if(err)
		goto err_free_mount;

	err = read_superblock(root, &used, &total);
	if(err)
		goto err_free_mount;

	data = path(root, "data");
	if(data == NULL)
		goto err_free_root;

	metadata = path(root, "metadata");
	if(metadata == NULL)
		goto err_free_data;

	*mount = ((struct humfs) { .total	= total,
				   .used	= used,
				   .data	= data,
				   .metadata	= metadata,
				   .mmap	= do_mmap });

	if(CHOOSE_MODE(do_mmap, 0)){
		printk("humfs doesn't support mmap in tt mode\n");
		do_mmap = 0;
	}

	*mount_data_out = mount;
	return(do_mmap ? &humfs_mmap_file_ops : &humfs_no_mmap_file_ops);

 err_free_data:
	kfree(data);
 err_free_root:
	kfree(root);
 err_free_mount:
	kfree(mount);
 err:
	return(NULL);
}

struct externfs_mount_ops humfs_mount_ops = {
	.mount			= mount_fs,
};

static int __init init_humfs(void)
{
	return(register_externfs("humfs", &humfs_mount_ops));
}

static void __exit exit_humfs(void)
{
	unregister_externfs("humfs");
}

__initcall(init_humfs);
__exitcall(exit_humfs);

/*
 * Overrides for Emacs so that we follow Linus's tabbing style.
 * Emacs will notice this stuff at the end of the file and automatically
 * adjust the settings for this buffer only.  This must remain at the end
 * of the file.
 * ---------------------------------------------------------------------------
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
