/* 
 * Copyright (C) 2000 - 2004 Jeff Dike (jdike@addtoit.com)
 * Licensed under the GPL
 */

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <utime.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/vfs.h>
#include "hostfs.h"
#include "kern_util.h"
#include "user.h"
#include "init.h"

/* Changed in hostfs_args before the kernel starts running */
static char *jail_dir = "/";
int append = 0;

static int __init hostfs_args(char *options, int *add)
{
	char *ptr;

	ptr = strchr(options, ',');
	if(ptr != NULL)
		*ptr++ = '\0';
	if(*options != '\0')
		jail_dir = options;

	options = ptr;
	while(options){
		ptr = strchr(options, ',');
		if(ptr != NULL)
			*ptr++ = '\0';
		if(*options != '\0'){
			if(!strcmp(options, "append"))
				append = 1;
			else printf("hostfs_args - unsupported option - %s\n",
				    options);
		}
		options = ptr;
	}
	return(0);
}

__uml_setup("hostfs=", hostfs_args,
"hostfs=<root dir>,<flags>,...\n"
"    This is used to set hostfs parameters.  The root directory argument\n"
"    is used to confine all hostfs mounts to within the specified directory\n"
"    tree on the host.  If this isn't specified, then a user inside UML can\n"
"    mount anything on the host that's accessible to the user that's running\n"
"    it.\n"
"    The only flag currently supported is 'append', which specifies that all\n"
"    files opened by hostfs will be opened in append mode.\n\n"
);

static int access_file(char *file, int uid, int gid, int r, int w, int x, 
		       void *mount)
{
	const char *path[] = { jail_dir, file, NULL };
	char tmp[HOSTFS_BUFSIZE];
	int err, mode = 0;

	if(r) mode = R_OK;
	if(w) mode |= W_OK;
	if(x) mode |= X_OK;
	
	err = -ENOMEM;
	file = get_path(path, tmp, sizeof(tmp));
	if(file == NULL)
		goto out;

	err = 0;
	if(access(file, mode) != 0)
		err = -errno;

	free_path(file, tmp);
 out:
	return(err);
}

static int mk_nod(const char *file, int uid, int gid, int ur, int uw, int ux, 
		  int gr, int gw, int gx, int or, int ow, int ox, 
		  int type, int dev, void *mount)
{
	const char *path[] = { jail_dir, file, NULL };
	char tmp[HOSTFS_BUFSIZE];
	int err = -ENOMEM;
	int mode = 0;

	file = get_path(path, tmp, sizeof(tmp));
	if(file == NULL)
		goto out;

	mode |= ur ? S_IRUSR : 0;
	mode |= uw ? S_IWUSR : 0;
	mode |= ux ? S_IXUSR : 0;
	mode |= gr ? S_IRGRP : 0;
	mode |= gw ? S_IWGRP : 0;
	mode |= gx ? S_IXGRP : 0;
	mode |= or ? S_IROTH : 0;
	mode |= ow ? S_IWOTH : 0;
	mode |= ox ? S_IXOTH : 0;

	/* XXX Pass type in an OS-independent way */
	mode |= type;

	err = mknod(file, mode, dev);
	if(err) 
		err = -errno;

	free_path(file, tmp);
 out:
	return(err);
}

static int stat_file(const char *file, void *mount, int *dev_out, 
		     unsigned long long *inode_out, int *mode_out, 
		     int *nlink_out, int *uid_out, int *gid_out, 
		     unsigned long long *size_out, 
		     unsigned long *atime_out, unsigned long *mtime_out, 
		     unsigned long *ctime_out, int *blksize_out, 
		     unsigned long long *blocks_out)
{
	const char *path[] = { jail_dir, file, NULL };

	return(host_stat_file(path, dev_out, inode_out, mode_out, nlink_out, 
			      uid_out, gid_out, size_out, atime_out, mtime_out, 
			      ctime_out, blksize_out, blocks_out));
}

static int file_type(const char *file, int *rdev, void *mount)
{
	const char *path[] = { jail_dir, file, NULL };

	return(host_file_type(path, rdev));
}

static int open_file(char *file, int uid, int gid, int r, int w, void *mount)
{
	const char *path[] = { jail_dir, file, NULL };

	return(host_open_file(path, r, w));
}

static void *open_dir(char *file, int uid, int gid, int *err_out, void *mount)
{
	const char *path[] = { jail_dir, file, NULL };

	return(host_open_dir(path, err_out));
}

static int read_file(int fd, unsigned long long offset, char *buf, int len,
		     void (*completion)(char *, int, void *), void *arg, 
		     void *mount)
{
	int err = host_read_file(fd, offset, buf, len);

	(*completion)(buf, err, arg);

	return(err);
}

static int file_create(char *file, int uid, int gid, int ur, int uw, int ux,
		       int gr, int gw, int gx, int or, int ow, int ox, 
		       void *mount)
{
	const char *path[] = { jail_dir, file, NULL };

	return(host_file_create(path, ur, uw, ux, gr, gw, gx, or, ow, ox));
}

static int set_attr(const char *file, struct hostfs_iattr *attrs, 
		    void *mount)
{
	const char *path[] = { jail_dir, file, NULL };

	return(host_set_attr(path, attrs));
}

static int make_symlink(const char *from, const char *to, int uid, int gid,
			void *mount)
{
	const char *path[] = { jail_dir, from, NULL };

	return(host_make_symlink(path, to));
}

static int unlink_file(const char *file, void *mount)
{
	const char *path[] = { jail_dir, file, NULL };

	return(host_unlink_file(path));
}

static int mk_dir(const char *file, int mode, int uid, int gid, void *mount)
{
	const char *path[] = { jail_dir, file, NULL };

	return(host_mkdir(path, mode));
}

static int rm_dir(const char *file, int uid, int gid, void *mount)
{
	const char *path[] = { jail_dir, file, NULL };

	return(host_rmdir(path));
}

static int link_file(const char *to, const char *from, int uid, int gid, 
		     void *mount)
{
	const char *to_path[] = { jail_dir, to, NULL };
	const char *from_path[] = { jail_dir, from, NULL };

	return(host_link_file(to_path, from_path));
}

static int read_link(char *file, int uid, int gid, char *buf, int size, 
		     void *mount)
{
	const char *path[] = { jail_dir, file, NULL };

	return(host_readlink(path, buf, size));
}

static int rename_file(char *from, char *to, void *mount)
{
	const char *to_path[] = { jail_dir, to, NULL };
	const char *from_path[] = { jail_dir, from, NULL };

	return(host_rename_file(to_path, from_path));
}

static int stat_fs(long *bsize_out, long long *blocks_out, 
		   long long *bfree_out, long long *bavail_out, 
		   long long *files_out, long long *ffree_out,
		   void *fsid_out, int fsid_size, long *namelen_out, 
		   long *spare_out, void *mount)
{
	const char *path[] = { jail_dir, mount, NULL };

	return(host_statfs(path, bsize_out, blocks_out, bfree_out, bavail_out, 
			   files_out, ffree_out, fsid_out, fsid_size, 
			   namelen_out, spare_out));
}

static struct externfs_file_ops hostfs_file_ops = {
	.stat_file		= stat_file,
	.file_type		= file_type,
	.access_file		= access_file,
	.open_file		= open_file,
	.open_dir		= open_dir,
	.read_dir		= generic_host_read_dir,
	.read_file		= read_file,
	.write_file		= generic_host_write_file,
	.map_file_page		= NULL,
	.close_file		= generic_host_close_file,
	.close_dir		= generic_host_close_dir,
	.file_create		= file_create,
	.set_attr		= set_attr,
	.make_symlink		= make_symlink,
	.unlink_file		= unlink_file,
	.mkdir			= mk_dir,
	.rmdir			= rm_dir,
	.mknod			= mk_nod,
	.link_file		= link_file,
	.readlink		= read_link,
	.rename_file		= rename_file,
	.statfs			= stat_fs,
	.truncate_file		= generic_host_truncate_file
};

static struct externfs_file_ops *mount_fs(char *mount_arg, 
					  void **mount_data_out)
{
	if(host_root_filename(mount_arg, mount_data_out))
		return(NULL);

	return(&hostfs_file_ops);
}

static struct externfs_mount_ops hostfs_mount_ops = {
	.mount			= mount_fs,
};

static int __init init_hostfs(void)
{
	return(register_externfs("hostfs", &hostfs_mount_ops));
}

static void __exit exit_hostfs(void)
{
	unregister_externfs("hostfs");
}

__initcall(init_hostfs);
__exitcall(exit_hostfs);

#if 0
module_init(init_hostfs)
module_exit(exit_hostfs)
MODULE_LICENSE("GPL");
#endif

/*
 * Overrides for Emacs so that we follow Linus's tabbing style.
 * Emacs will notice this stuff at the end of the file and automatically
 * adjust the settings for this buffer only.  This must remain at the end
 * of the file.
 * ---------------------------------------------------------------------------
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
