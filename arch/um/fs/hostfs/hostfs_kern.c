/* 
 * Copyright (C) 2000 - 2004 Jeff Dike (jdike@addtoit.com)
 * Licensed under the GPL
 */

#include <linux/stddef.h>
#include <linux/fs.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/pagemap.h>
#include <linux/blkdev.h>
#include <asm/uaccess.h>
#include "hostfs.h"
#include "kern_util.h"
#include "kern.h"
#include "user_util.h"
#include "2_5compat.h"
#include "init.h"
#include "mem.h"

struct externfs {
	struct list_head list;
	struct externfs_mount_ops *ops;
	struct file_system_type type;
};

#define file_hostfs_i(file) (&(file)->f_dentry->d_inode->u.hostfs_i)

int hostfs_d_delete(struct dentry *dentry)
{
	return(1);
}

struct dentry_operations hostfs_dentry_ops = {
	.d_delete		= hostfs_d_delete,
};

#define HOSTFS_SUPER_MAGIC 0x00c0ffee

static struct inode_operations hostfs_iops;
static struct inode_operations hostfs_dir_iops;
static struct address_space_operations hostfs_link_aops;

static char *dentry_name(struct dentry *dentry, int extra)
{
	struct dentry *parent;
	char *name;
	int len;

	len = 0;
	parent = dentry;
	while(parent->d_parent != parent){
		len += parent->d_name.len + 1;
		parent = parent->d_parent;
	}
	
	name = kmalloc(len + extra + 1, GFP_KERNEL);
	if(name == NULL) return(NULL);

	name[len] = '\0';
	parent = dentry;
	while(parent->d_parent != parent){
		len -= parent->d_name.len + 1;
		name[len] = '/';
		strncpy(&name[len + 1], parent->d_name.name, 
			parent->d_name.len);
		parent = parent->d_parent;
	}

	return(name);
}

static char *inode_name(struct inode *ino, int extra)
{
	struct dentry *dentry;

	dentry = list_entry(ino->i_dentry.next, struct dentry, d_alias);
	return(dentry_name(dentry, extra));
}

static int read_name(struct inode *ino, char *name)
{
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	/* The non-int inode fields are copied into ints by stat_file and
	 * then copied into the inode because passing the actual pointers
	 * in and having them treated as int * breaks on big-endian machines
	 */
	int err;
	int i_dev, i_mode, i_nlink, i_blksize;
	unsigned long long i_size;
	unsigned long long i_ino;
	unsigned long long i_blocks;

	err = (*ops->stat_file)(name, ino->i_sb->u.generic_sbp, &i_dev, &i_ino,
				&i_mode, &i_nlink, &ino->i_uid, &ino->i_gid,
				&i_size, &ino->i_atime, &ino->i_mtime, 
				&ino->i_ctime, &i_blksize, &i_blocks);
	if(err) return(err);
	ino->i_ino = i_ino;
	ino->i_dev = i_dev;
	ino->i_mode = i_mode;
	ino->i_nlink = i_nlink;
	ino->i_size = i_size;
	ino->i_blksize = i_blksize;
	ino->i_blocks = i_blocks;
	return(0);
}

static char *follow_link(char *link, 
			 int (*do_readlink)(char *path, int uid, int gid,
					    char *buf, int size, void *mount),
			 int uid, int gid, void *mount)
{
	int len, n;
	char *name, *resolved, *end;

	len = 64;
	while(1){
		n = -ENOMEM;
		name = kmalloc(len, GFP_KERNEL);
		if(name == NULL)
			goto out;

		n = (*do_readlink)(link, uid, gid, name, len, mount);
		if(n < len)
			break;
		len *= 2;
		kfree(name);
	}
	if(n < 0)
		goto out_free;

	if(*name == '/')
		return(name);

	end = strrchr(link, '/');
	if(end == NULL)
		return(name);

	*(end + 1) = '\0';
	len = strlen(link) + strlen(name) + 1;

	resolved = kmalloc(len, GFP_KERNEL);
	if(resolved == NULL){
		n = -ENOMEM;
		goto out_free;
	}

	sprintf(resolved, "%s%s", link, name);
	kfree(name);
	kfree(link);
	return(resolved);

 out_free:
	kfree(name);
 out:
	return(ERR_PTR(n));
}

static int read_inode(struct inode *ino)
{
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	void *mount = ino->i_sb->u.generic_sbp;
	char *name;
	int err, type;

	err = -ENOMEM;
	name = inode_name(ino, 0);
	if(name == NULL) 
		goto out;

	type = (*ops->file_type)(name, NULL, mount);
	if(type < 0){
		err = type;
		goto out;
	}

	if(type == OS_TYPE_SYMLINK){
		name = follow_link(name, ops->readlink, current->fsuid,
				   current->fsgid, mount);
		if(IS_ERR(name)){
			err = PTR_ERR(name);
			goto out;
		}
	}
	
	err = read_name(ino, name);
	kfree(name);
 out:
	return(err);
}

void hostfs_delete_inode(struct inode *ino)
{
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	void *mount = ino->i_sb->u.generic_sbp;

	if(ino->u.hostfs_i.fd != -1) 
		(*ops->close_file)(&ino->u.hostfs_i.fd, ino->i_size, mount);

	ino->u.hostfs_i.mode = 0;
	clear_inode(ino);
}

int hostfs_statfs(struct super_block *sb, struct statfs *sf)
{
	/* do_statfs uses struct statfs64 internally, but the linux kernel
	 * struct statfs still has 32-bit versions for most of these fields,
	 * so we convert them here
	 */
	int err;
	long long f_blocks;
	long long f_bfree;
	long long f_bavail;
	long long f_files;
	long long f_ffree;
	struct inode *ino = sb->s_root->d_inode;
	void *mount = sb->u.generic_sbp;
	
	err = (*ino->u.hostfs_i.ops->statfs)(&sf->f_bsize, &f_blocks, 
					     &f_bfree, &f_bavail, &f_files, 
					     &f_ffree, &sf->f_fsid, 
					     sizeof(sf->f_fsid), 
					     &sf->f_namelen, sf->f_spare, 
					     mount);
	if(err) return(err);
	sf->f_blocks = f_blocks;
	sf->f_bfree = f_bfree;
	sf->f_bavail = f_bavail;
	sf->f_files = f_files;
	sf->f_ffree = f_ffree;
	sf->f_type = HOSTFS_SUPER_MAGIC;
	return(0);
}

static struct super_operations hostfs_sbops = { 
	.put_inode	= force_delete,
	.delete_inode	= hostfs_delete_inode,
	.statfs		= hostfs_statfs,
};

int hostfs_readdir(struct file *file, void *ent, filldir_t filldir)
{
	void *dir;
	char *name;
	unsigned long long next, ino;
	int error, len;
	struct externfs_file_ops *ops = file_hostfs_i(file)->ops;
	void *mount = file->f_dentry->d_inode->i_sb->u.generic_sbp;

	name = dentry_name(file->f_dentry, 0);
	if(name == NULL) return(-ENOMEM);
	dir = (*ops->open_dir)(name, current->fsuid, current->fsgid, &error, 
			       mount);
	kfree(name);
	if(dir == NULL) return(-error);
	next = file->f_pos;
	while((name = (*ops->read_dir)(dir, &next, &ino, &len, mount)) != NULL){
		error = (*filldir)(ent, name, len, file->f_pos, 
				   ino, DT_UNKNOWN);
		if(error) break;
		file->f_pos = next;
	}
	(*ops->close_dir)(dir, mount);
	return(0);
}

int hostfs_file_open(struct inode *ino, struct file *file)
{
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	void *mount = ino->i_sb->u.generic_sbp;
	char *name;
	int mode = 0, r = 0, w = 0, fd;

	mode = file->f_mode & (FMODE_READ | FMODE_WRITE);
	if((mode & ino->u.hostfs_i.mode) == mode)
		return(0);

	/* The file may already have been opened, but with the wrong access,
	 * so this resets things and reopens the file with the new access.
	 */
	if(ino->u.hostfs_i.fd != -1){
		(*ops->close_file)(&ino->u.hostfs_i.fd, ino->i_size, mount);
		ino->u.hostfs_i.fd = -1;
	}

	ino->u.hostfs_i.mode |= mode;
	if(ino->u.hostfs_i.mode & FMODE_READ) 
		r = 1;
	if(ino->u.hostfs_i.mode & FMODE_WRITE) 
		w = 1;
	if(w) 
		r = 1;

	name = dentry_name(file->f_dentry, 0);
	if(name == NULL) 
		return(-ENOMEM);

	fd = (*ops->open_file)(name, current->fsuid, current->fsgid, r, w, 
			       mount);
	kfree(name);
	if(fd < 0) return(fd);
	file_hostfs_i(file)->fd = fd;

	return(0);
}

int hostfs_dir_open(struct inode *ino, struct file *file)
{
	return(0);	
}

int hostfs_dir_release(struct inode *ino, struct file *file)
{
	return(0);
}

int hostfs_fsync(struct file *file, struct dentry *dentry, int datasync)
{
	return(0);
}

static struct file_operations hostfs_file_fops = {
	.owner		= NULL,
	.read		= generic_file_read,
	.write		= generic_file_write,
	.mmap		= generic_file_mmap,
	.open		= hostfs_file_open,
	.release	= NULL,
	.fsync		= hostfs_fsync,
};

static struct file_operations hostfs_dir_fops = {
	.owner		= NULL,
	.readdir	= hostfs_readdir,
	.open		= hostfs_dir_open,
	.release	= hostfs_dir_release,
	.fsync		= hostfs_fsync,
};

int hostfs_writepage(struct page *page)
{
	struct address_space *mapping = page->mapping;
	struct inode *inode = mapping->host;
	struct externfs_file_ops *ops = inode->u.hostfs_i.ops;
	void *mount = inode->i_sb->u.generic_sbp;
	char *buffer;
	unsigned long long base;
	int count = PAGE_CACHE_SIZE;
	int end_index = inode->i_size >> PAGE_CACHE_SHIFT;
	int err;

	if (page->index >= end_index)
		count = inode->i_size & (PAGE_CACHE_SIZE-1);

	buffer = kmap(page);
	base = ((unsigned long long) page->index) << PAGE_CACHE_SHIFT;

	err = (*ops->write_file)(inode->u.hostfs_i.fd, base, buffer, count,
				 mount);
	if(err != count){
		ClearPageUptodate(page);
		goto out;
	}
	base += count;

	if (base > inode->i_size)
		inode->i_size = base;

	if (PageError(page))
		ClearPageError(page);	
	err = 0;

 out:	
	kunmap(page);

	UnlockPage(page);
	return err; 
}

static void hostfs_finish_readpage(char *buffer, int res, void *arg)
{
	struct page *page = arg;

	if(res < 0){
		SetPageError(page);
		goto out;
	}

	memset(&buffer[res], 0, PAGE_CACHE_SIZE - res);

	flush_dcache_page(page);
	SetPageUptodate(page);
	if (PageError(page)) 
		ClearPageError(page);
 out:
	kunmap(page);
	UnlockPage(page);
}

static int hostfs_readpage(struct file *file, struct page *page)
{
	struct inode *ino = page->mapping->host;
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	void *mount = ino->i_sb->u.generic_sbp;
	char *buffer;
	long long start;
	int err = 0;

	start = (long long) page->index << PAGE_CACHE_SHIFT;
	buffer = kmap(page);

	if(ops->map_file_page != NULL){
		/* XXX What happens when PAGE_SIZE != PAGE_CACHE_SIZE? */
		err = (*ops->map_file_page)(file_hostfs_i(file)->fd, start, 
					    buffer, file->f_mode & FMODE_WRITE,
					    mount);
		if(!err)
			err = PAGE_CACHE_SIZE;
	}
	else err = (*ops->read_file)(file_hostfs_i(file)->fd, start, buffer,
				     PAGE_CACHE_SIZE, hostfs_finish_readpage,
				     page, mount);

	if(err > 0)
		err = 0;
	return(err);
}

struct writepage_info {
	struct semaphore sem;
	int res;
};

static void hostfs_finish_prepare(char *buffer, int res, void *arg)
{
	struct writepage_info *wp = arg;

	wp->res = res;
	up(&wp->sem);
}

int hostfs_prepare_write(struct file *file, struct page *page, 
			 unsigned int from, unsigned int to)
{
	struct address_space *mapping = page->mapping;
	struct inode *inode = mapping->host;
	struct externfs_file_ops *ops = inode->u.hostfs_i.ops;
	void *mount = inode->i_sb->u.generic_sbp;
	char *buffer;
	long long start;
	int err;
	struct writepage_info wp;

	start = (long long) page->index << PAGE_CACHE_SHIFT;
	buffer = kmap(page);

	if(ops->map_file_page != NULL){
		err = (*ops->map_file_page)(file_hostfs_i(file)->fd, start, 
					    buffer, file->f_mode & FMODE_WRITE,
					    mount);
		goto out;
		
	}

	if(from != 0){
		init_MUTEX_LOCKED(&wp.sem);
		err = (*ops->read_file)(file_hostfs_i(file)->fd, start, buffer,
					from, hostfs_finish_prepare, &wp, 
					mount);
		down(&wp.sem);
		if(err < 0) 
			goto out;

		err = wp.res;
		if(err < 0)
			goto out;
	}
	if(to != PAGE_CACHE_SIZE){
		start += to;
		init_MUTEX_LOCKED(&wp.sem);
		err = (*ops->read_file)(file_hostfs_i(file)->fd, start, 
					buffer + to, PAGE_CACHE_SIZE - to,
					hostfs_finish_prepare, &wp, mount);
		down(&wp.sem);
		if(err < 0) 
			goto out;

		err = wp.res;
		if(err < 0)
			goto out;
	}
	err = 0;
 out:
	kunmap(page);
	return(err);
}

static int hostfs_commit_write(struct file *file, struct page *page, 
			       unsigned from, unsigned to)
{
	struct address_space *mapping = page->mapping;
	struct inode *inode = mapping->host;
	struct externfs_file_ops *ops = inode->u.hostfs_i.ops;
	void *mount = inode->i_sb->u.generic_sbp;
	char *buffer;
	long long start;
	int err;

	start = (long long) (page->index << PAGE_CACHE_SHIFT) + from;

	if(ops->map_file_page != NULL)
		err = to - from;
	else {
		buffer = kmap(page);
		err = (*ops->write_file)(file_hostfs_i(file)->fd, start, 
					 buffer + from, to - from, mount);
		if(err < 0) 
			goto out;
	}

	start += err;
	err = 0;
	if(start > inode->i_size)
		inode->i_size = start;

 out:
	kunmap(page);
	return(err);
}

static void hostfs_removepage(struct page *page)
{
	physmem_remove_mapping(page_address(page));
}

static struct address_space_operations hostfs_aops = {
	.writepage 	= hostfs_writepage,
	.readpage	= hostfs_readpage,
	.removepage	= hostfs_removepage,
/* 	.set_page_dirty = __set_page_dirty_nobuffers, */
	.prepare_write	= hostfs_prepare_write,
	.commit_write	= hostfs_commit_write
};

static struct externfs *find_externfs(struct file_system_type *type);

static struct inode *get_inode(struct super_block *sb, struct dentry *dentry,
			       struct externfs_file_ops *ops, int *error)
{
	struct inode *inode;
	char *name;
	int type, err = -ENOMEM, rdev;

	inode = new_inode(sb);
	if(inode == NULL) 
		goto out;

	inode->u.hostfs_i.fd = -1;
	inode->u.hostfs_i.mode = 0;
	inode->u.hostfs_i.ops = ops;
	insert_inode_hash(inode);
	if(dentry){
		name = dentry_name(dentry, 0);
		if(name == NULL){
			err = -ENOMEM;
			goto out_put;
		}
		type = (*ops->file_type)(name, &rdev, sb->u.generic_sbp);
		kfree(name);
	}
	else type = OS_TYPE_DIR;
	inode->i_sb = sb;

	err = 0;
	if(type == OS_TYPE_SYMLINK)
		inode->i_op = &page_symlink_inode_operations;
	else if(type == OS_TYPE_DIR)
		inode->i_op = &hostfs_dir_iops;
	else inode->i_op = &hostfs_iops;

	if(type == OS_TYPE_DIR) inode->i_fop = &hostfs_dir_fops;
	else inode->i_fop = &hostfs_file_fops;

	if(type == OS_TYPE_SYMLINK) 
		inode->i_mapping->a_ops = &hostfs_link_aops;
	else inode->i_mapping->a_ops = &hostfs_aops;

	switch (type) {
	case OS_TYPE_CHARDEV:
		init_special_inode(inode, S_IFCHR, rdev);
		break;
	case OS_TYPE_BLOCKDEV:
		init_special_inode(inode, S_IFBLK, rdev);
		break;
	case OS_TYPE_FIFO:
		init_special_inode(inode, S_IFIFO, 0);
		break;
	case OS_TYPE_SOCK:
		init_special_inode(inode, S_IFSOCK, 0);
		break;
	}
	
	if(error) *error = err;
	return(inode);
 out_put:
	make_bad_inode(inode);
	iput(inode);
 out:
	if(error) *error = err;
	return(NULL);
}

int hostfs_create(struct inode *dir, struct dentry *dentry, int mode)
{
	struct externfs_file_ops *ops = dir->u.hostfs_i.ops;
	struct inode *inode;
	void *mount = dir->i_sb->u.generic_sbp;
	char *name;
	int error, fd;

	inode = get_inode(dir->i_sb, dentry, ops, &error);
	if(error) return(error);
	name = dentry_name(dentry, 0);
	if(name == NULL){
		iput(inode);
		return(-ENOMEM);
	}
	fd = (*ops->file_create)(name, current->fsuid, current->fsuid,
				 mode & S_IRUSR, mode & S_IWUSR, 
				 mode & S_IXUSR, mode & S_IRGRP, 
				 mode & S_IWGRP, mode & S_IXGRP, 
				 mode & S_IROTH, mode & S_IWOTH, 
				 mode & S_IXOTH, mount);
	if(fd < 0) 
		error = fd;
	else error = read_name(inode, name);

	kfree(name);
	if(error){
		iput(inode);
		return(error);
	}
	inode->u.hostfs_i.fd = fd;
	inode->u.hostfs_i.mode = FMODE_READ | FMODE_WRITE;
	d_instantiate(dentry, inode);
	return(0);
}
 
struct dentry *hostfs_lookup(struct inode *ino, struct dentry *dentry)
{
	struct inode *inode;
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	char *name;
	int error;

	inode = get_inode(ino->i_sb, dentry, ops, &error);
	if(error != 0) return(ERR_PTR(error));
	name = dentry_name(dentry, 0);
	if(name == NULL) return(ERR_PTR(-ENOMEM));
	error = read_name(inode, name);
	kfree(name);
	if(error){
		iput(inode);
		if(error == -ENOENT) inode = NULL;
		else return(ERR_PTR(error));
	}
	d_add(dentry, inode);
	dentry->d_op = &hostfs_dentry_ops;
	return(NULL);
}

static char *inode_dentry_name(struct inode *ino, struct dentry *dentry)
{
        char *file;
	int len;

	file = inode_name(ino, dentry->d_name.len + 1);
	if(file == NULL) return(NULL);
        strcat(file, "/");
	len = strlen(file);
        strncat(file, dentry->d_name.name, dentry->d_name.len);
	file[len + dentry->d_name.len] = '\0';
        return(file);
}

int hostfs_link(struct dentry *to, struct inode *ino, struct dentry *from)
{
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	void *mount = ino->i_sb->u.generic_sbp;
        char *from_name, *to_name;
        int err;

        from_name = inode_dentry_name(ino, from); 
        if(from_name == NULL) 
                return(-ENOMEM);
        to_name = dentry_name(to, 0);
	if(to_name == NULL){
		kfree(from_name);
		return(-ENOMEM);
	}
        err = (*ops->link_file)(to_name, from_name, current->fsuid, 
				current->fsgid, mount);
        kfree(from_name);
        kfree(to_name);
        return(err);
}

int hostfs_unlink(struct inode *ino, struct dentry *dentry)
{
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	void *mount = ino->i_sb->u.generic_sbp;
	char *file;
	int err;

	file = inode_dentry_name(ino, dentry);
	if(file == NULL) 
		return(-ENOMEM);

	err = (*ops->unlink_file)(file, mount);
	kfree(file);
	return(err);
}

int hostfs_symlink(struct inode *ino, struct dentry *dentry, const char *to)
{
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	void *mount = ino->i_sb->u.generic_sbp;
	char *file;
	int err;

	file = inode_dentry_name(ino, dentry);
	if(file == NULL) 
		return(-ENOMEM);
	err = (*ops->make_symlink)(file, to, current->fsuid, current->fsgid,
				   mount);
	kfree(file);
	return(err);
}

int hostfs_mkdir(struct inode *ino, struct dentry *dentry, int mode)
{
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	void *mount = ino->i_sb->u.generic_sbp;
	char *file;
	int err;

	file = inode_dentry_name(ino, dentry);
	if(file == NULL) 
		return(-ENOMEM);
	err = (*ops->mkdir)(file, mode, current->fsuid, current->fsgid, mount);
	kfree(file);
	return(err);
}

int hostfs_rmdir(struct inode *ino, struct dentry *dentry)
{
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	void *mount = ino->i_sb->u.generic_sbp;
	char *file;
	int err;

	file = inode_dentry_name(ino, dentry);
	if(file == NULL) return(-ENOMEM);
	err = (*ops->rmdir)(file, current->fsuid, current->fsgid, mount);
	kfree(file);
	return(err);
}

int hostfs_mknod(struct inode *dir, struct dentry *dentry, int mode, int dev)
{
	struct externfs_file_ops *ops = dir->u.hostfs_i.ops;
	void *mount = dir->i_sb->u.generic_sbp;
	struct inode *inode;
	char *name;
	int error;
 
	inode = get_inode(dir->i_sb, dentry, ops, &error);
	if(error) 
		return(error);
	name = dentry_name(dentry, 0);
	if(name == NULL){
		iput(inode);
		return(-ENOMEM);
	}
	init_special_inode(inode, mode, dev);
	error = (*ops->mknod)(name, current->fsuid, current->fsgid, 
			      mode & S_IRUSR, mode & S_IWUSR, 
			      mode & S_IXUSR, mode & S_IRGRP, 
			      mode & S_IWGRP, mode & S_IXGRP, 
			      mode & S_IROTH, mode & S_IWOTH, 
			      mode & S_IXOTH, mode & S_IFMT, dev, mount);
	if(!error)
		error = read_name(inode, name);
	kfree(name);
	if(error){
		iput(inode);
		return(error);
	}
	d_instantiate(dentry, inode);
	return(0);
}

int hostfs_rename(struct inode *from_ino, struct dentry *from,
		  struct inode *to_ino, struct dentry *to)
{
	struct externfs_file_ops *ops = from_ino->u.hostfs_i.ops;
	void *mount = from_ino->i_sb->u.generic_sbp;
	char *from_name, *to_name;
	int err;

	from_name = inode_dentry_name(from_ino, from);
	if(from_name == NULL)
		return(-ENOMEM);
	to_name = inode_dentry_name(to_ino, to);
	if(to_name == NULL){
		kfree(from_name);
		return(-ENOMEM);
	}
	err = (*ops->rename_file)(from_name, to_name, mount);
	kfree(from_name);
	kfree(to_name);
	return(err);
}

void hostfs_truncate(struct inode *ino)
{
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	void *mount = ino->i_sb->u.generic_sbp;

	(*ops->truncate_file)(ino->u.hostfs_i.fd, ino->i_size, mount);
}

int hostfs_permission(struct inode *ino, int desired)
{
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	void *mount = ino->i_sb->u.generic_sbp;
	char *name;
	int r = 0, w = 0, x = 0, err;

	if(ops->access_file == NULL)
		return(vfs_permission(ino, desired));

	if(desired & MAY_READ) r = 1;
	if(desired & MAY_WRITE) w = 1;
	if(desired & MAY_EXEC) x = 1;
	name = inode_name(ino, 0);
	if(name == NULL) 
		return(-ENOMEM);

	err = (*ops->access_file)(name, r, w, x, current->fsuid, current->fsgid,
				  mount);
	kfree(name);

	if(!err) 
		err = vfs_permission(ino, desired);
	return(err);
}

int hostfs_setattr(struct dentry *dentry, struct iattr *attr)
{
	struct externfs_file_ops *ops = dentry->d_inode->u.hostfs_i.ops;
	void *mount = dentry->d_inode->i_sb->u.generic_sbp;
	struct hostfs_iattr attrs;
	char *name;
	int err;
	
	attrs.ia_valid = 0;
	if(attr->ia_valid & ATTR_MODE){
		attrs.ia_valid |= HOSTFS_ATTR_MODE;
		attrs.ia_mode = attr->ia_mode;
	}
	if(attr->ia_valid & ATTR_UID){
		attrs.ia_valid |= HOSTFS_ATTR_UID;
		attrs.ia_uid = attr->ia_uid;
	}
	if(attr->ia_valid & ATTR_GID){
		attrs.ia_valid |= HOSTFS_ATTR_GID;
		attrs.ia_gid = attr->ia_gid;
	}
	if(attr->ia_valid & ATTR_SIZE){
		attrs.ia_valid |= HOSTFS_ATTR_SIZE;
		attrs.ia_size = attr->ia_size;
	}
	if(attr->ia_valid & ATTR_ATIME){
		attrs.ia_valid |= HOSTFS_ATTR_ATIME;
		attrs.ia_atime = attr->ia_atime;
	}
	if(attr->ia_valid & ATTR_MTIME){
		attrs.ia_valid |= HOSTFS_ATTR_MTIME;
		attrs.ia_mtime = attr->ia_mtime;
	}
	if(attr->ia_valid & ATTR_CTIME){
		attrs.ia_valid |= HOSTFS_ATTR_CTIME;
		attrs.ia_ctime = attr->ia_ctime;
	}
	if(attr->ia_valid & ATTR_ATIME_SET){
		attrs.ia_valid |= HOSTFS_ATTR_ATIME_SET;
	}
	if(attr->ia_valid & ATTR_MTIME_SET){
		attrs.ia_valid |= HOSTFS_ATTR_MTIME_SET;
	}
	name = dentry_name(dentry, 0);
	if(name == NULL) 
		return(-ENOMEM);
	err = (*ops->set_attr)(name, &attrs, mount);
	kfree(name);
	if(err)
		return(err);

	return(inode_setattr(dentry->d_inode, attr));
}

int hostfs_getattr(struct dentry *dentry, struct iattr *attr)
{
	not_implemented();
	return(-EINVAL);
}

static struct inode_operations hostfs_iops = {
	.create		= hostfs_create,
	.link		= hostfs_link,
	.unlink		= hostfs_unlink,
	.symlink	= hostfs_symlink,
	.mkdir		= hostfs_mkdir,
	.rmdir		= hostfs_rmdir,
	.mknod		= hostfs_mknod,
	.rename		= hostfs_rename,
	.truncate	= hostfs_truncate,
	.permission	= hostfs_permission,
	.setattr	= hostfs_setattr,
	.getattr	= hostfs_getattr,
};

static struct inode_operations hostfs_dir_iops = {
	.create		= hostfs_create,
	.lookup		= hostfs_lookup,
	.link		= hostfs_link,
	.unlink		= hostfs_unlink,
	.symlink	= hostfs_symlink,
	.mkdir		= hostfs_mkdir,
	.rmdir		= hostfs_rmdir,
	.mknod		= hostfs_mknod,
	.rename		= hostfs_rename,
	.truncate	= hostfs_truncate,
	.permission	= hostfs_permission,
	.setattr	= hostfs_setattr,
	.getattr	= hostfs_getattr,
};

int hostfs_link_readpage(struct file *file, struct page *page)
{
	struct inode *ino = page->mapping->host;
	struct externfs_file_ops *ops = ino->u.hostfs_i.ops;
	void *mount = ino->i_sb->u.generic_sbp;
	char *buffer, *name;
	long long start;
	int err;

	start = page->index << PAGE_CACHE_SHIFT;
	buffer = kmap(page);
	name = inode_name(ino, 0);
	if(name == NULL) return(-ENOMEM);
	err = (*ops->readlink)(name, current->fsuid, current->fsgid, buffer, 
			       PAGE_CACHE_SIZE, mount);
	kfree(name);
	if(err == PAGE_CACHE_SIZE)
		err = -E2BIG;
	else if(err > 0){
		flush_dcache_page(page);
		SetPageUptodate(page);
		if (PageError(page)) ClearPageError(page);
		err = 0;
	}
	kunmap(page);
	UnlockPage(page);
	return(err);
}

static struct address_space_operations hostfs_link_aops = {
	.readpage	= hostfs_link_readpage,
	.removepage	= hostfs_removepage,
};

DECLARE_MUTEX(externfs_sem);
struct list_head externfses = LIST_HEAD_INIT(externfses);

static struct externfs *find_externfs(struct file_system_type *type)
{
	struct list_head *ele;
	struct externfs *fs;

	down(&externfs_sem);
	list_for_each(ele, &externfses){
		fs = list_entry(ele, struct externfs, list);
		if(&fs->type == type)
			goto out;
	}
	fs = NULL;
 out:
	up(&externfs_sem);
	return(fs);
}

#define DEFAULT_ROOT "/"

int host_root_filename(char *mount_arg, void **mount_data_out)
{
	*mount_data_out = NULL;

	if((mount_arg == NULL) || (*mount_arg == '\0'))
		*mount_data_out = DEFAULT_ROOT;
	else *mount_data_out = mount_arg;

	return(0);
}

int host_read_file(int fd, unsigned long long offset, char *buf, int len)
{
	int err;

	err = os_seek_file(fd, offset);
	if(err)
		return(err);

	return(os_read_file(fd, buf, len));
}

int host_write_file(int fd, unsigned long long offset, const char *buf, 
		    int len)
{
	int err;

	err = os_seek_file(fd, offset);
	if(err)
		return(err);

	return(os_write_file(fd, buf, len));
}

void host_close_file(void *stream)
{
	os_close_file(*((int *) stream));
}

struct super_block *hostfs_read_super(struct super_block *sb, void *data, 
				      int silent)
{
	struct externfs *fs;
	struct inode *root_inode;
	struct externfs_file_ops *file_ops;

	sb->s_blocksize = 1024;
	sb->s_blocksize_bits = 10;
	sb->s_magic = HOSTFS_SUPER_MAGIC;
	sb->s_op = &hostfs_sbops;

	fs = find_externfs(sb->s_type);
	if(fs == NULL){
		printk("Couldn't find externfs for filesystem '%s'\n",
		       sb->s_type->name);
		goto out;
	}

	file_ops = (*fs->ops->mount)(data, &sb->u.generic_sbp);
	if(file_ops == NULL)
		goto out;

	root_inode = get_inode(sb, NULL, file_ops, NULL);
	if(root_inode == NULL)
		goto out;
	root_inode->u.hostfs_i.ops = file_ops;

	sb->s_root = d_alloc_root(root_inode);
	if(sb->s_root == NULL)
		goto out_put;

	if(read_inode(root_inode))
		goto out_dput;
	return(sb);

 out_dput:
	/* dput frees the inode */
	dput(sb->s_root);
	return(NULL);
 out_put:
	make_bad_inode(root_inode);
	iput(root_inode);
 out:
	return(NULL);
}

int register_externfs(char *name, struct externfs_mount_ops *ops)
{
	struct externfs *new;
	int err = -ENOMEM;

	new = kmalloc(sizeof(*new), GFP_KERNEL);
	if(new == NULL)
		goto out;

	memset(new, 0, sizeof(*new));
	*new = ((struct externfs) { .list	= LIST_HEAD_INIT(new->list),
				    .ops	= ops,
				    .type = { .name	= name,
					      .read_super = hostfs_read_super,
					      .fs_flags	= 0,
					      .owner	= THIS_MODULE } });
	list_add(&new->list, &externfses);

	err = register_filesystem(&new->type);
	if(err)
		goto out_del;
	return(0);

 out_del:
	list_del(&new->list);
	kfree(new);
 out:
	return(err);
}

void unregister_externfs(char *name)
{
	struct list_head *ele;
	struct externfs *fs;

	down(&externfs_sem);
	list_for_each(ele, &externfses){
		fs = list_entry(ele, struct externfs, list);
		if(!strcmp(fs->type.name, name)){
			list_del(ele);
			up(&externfs_sem);
			return;
		}
	}
	up(&externfs_sem);
	printk("Unregister_externfs - filesystem '%s' not found\n", name);
}

/*
 * Overrides for Emacs so that we follow Linus's tabbing style.
 * Emacs will notice this stuff at the end of the file and automatically
 * adjust the settings for this buffer only.  This must remain at the end
 * of the file.
 * ---------------------------------------------------------------------------
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
